import vue.*;
import controleur.*;
import modele.*;
public class Main {
    public static void main(String [] args){
      // System.out.println("Main 1");
        Modele m=new Modele();
        // System.out.println("Main 2");
        Controleur controleur = new Controleur(m);
        // System.out.println("Main 3");
        Vue vue = new Vue(controleur, m);
        // System.out.println("Main 4");
        vue.setVisible(true);
        // System.out.println("Main 5 ");
    }
}
