package modele;
import com.jogamp.opengl.GL2;
public abstract class Objet3D{
  protected int[] posinit={0,0,0};
  protected double phi=0;
  protected double theta=0;
  protected double rayon=1;


  public abstract void init(GL2 gl); // initialisation
  public abstract void appliqueChangementRepere(GL2 gl); // Applique les transformations à effectuer avant l'affichage de l'objet
  public abstract void update(); // permet de faire évoluer l'objet
  public abstract void affiche(GL2 gl);

  public  void position(int x,int y,int z){
    posinit[0]=x;
    posinit[1]=y;
    posinit[2]=z;
  };

}
