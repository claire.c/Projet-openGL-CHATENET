package modele;
import com.jogamp.opengl.GL2;

public class Pale extends Maillage3D {
    private Double angle = 180., excentreZ=33., excentreY=7.;
    private Moulin monMoulin;
  public Pale(String jsonString, Moulin moulin) {
      super(jsonString);
      monMoulin = moulin;
      setTexture("./windmill/material/moulin.jpg");

  }


  @Override
  public void appliqueChangementRepere (GL2 gl){
      monMoulin.appliqueChangementRepere(gl);
      gl.glTranslated(0,-excentreY,excentreZ);
      gl.glRotated(angle,1,0,0);
      gl.glTranslated(0,excentreY,-excentreZ);
  }


  public void update(){
    this.angle += 0.1;
  }
}
