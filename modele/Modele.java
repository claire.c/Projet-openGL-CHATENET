package modele;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import org.json.*;
import java.io.*;
import java.lang.Math;
import java.util.Scanner;

public class Modele{

  Objet3D[] objets;
  String jsonString;
  private class Camera{
    double x=-7,y=30,z=80;
    double dirX=0,dirY=30,dirZ=0;
    double upX=0,upY=1,upZ=0;

    Camera(){}

    void positionner(GL2 gl, GLU glu){
      gl.glLoadIdentity();
      glu.gluLookAt ( x,y,z ,dirX,dirY,dirZ,upX,upY,upZ);
    }

    void deplacer(double dx, double dy, double dz){
      x+=dx;
      dirX+=dx;
      z+=dz;
      dirZ+=dz;
      y+=dy;
      dirY+=dy;
    }
  } //fermeture de la classe camera

  Camera cam=new Camera();
  public  void deplacerCam(double dx, double dy, double dz){
      cam.deplacer(dx,dy,dz);
  }

  public void positionnerCam(GL2 gl, GLU glu){
    cam.positionner(gl, glu);
  }

  public Modele(){

    try {
      jsonString = new Scanner(new File("moulin.json")).useDelimiter("\\Z").next();
      // System.out.println("Fin de la création du jsonString : "+jsonString);
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    // System.out.println("thread #1 : "+Thread.currentThread().getName());
    // System.out.println("1");
    objets = new Objet3D[2];
    // System.out.println("2");
    objets[0] = new Repere();
    // System.out.println("3");
    objets[1] = new Moulin(jsonString);
    // System.out.println("4");






  }

  public void init(GL2 gl){
    for (Objet3D objet : objets)
      objet.init(gl);
  }

  public Objet3D[] getObjets(){
    return objets;
  }

  public void update(){
    for (Objet3D objet : objets)
      objet.update();
  }





}
