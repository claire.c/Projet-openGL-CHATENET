package modele;
import com.jogamp.opengl.GL2;
import java.nio.*;
import com.jogamp.common.nio.Buffers;
import com.jogamp.common.nio.PointerBuffer;
import java.io.*;
import org.json.*;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import java.lang.Math;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Random;
import java.util.Scanner;

public class Maillage3D extends Objet3D {
  String jsonString;
  float[] sommets;
  int[] tabIndex;
  int nbIndex;
  FloatBuffer vertexFB;
  IntBuffer indexBuf;
  DoubleBuffer textures;

  protected int angle = 0, Xangle = 0;
  protected double speed = 5., Xspeed = 0;
  private String texture;
  protected Texture newTex;



  public Maillage3D(String jsonString){
    // System.out.println("thread #3 : "+Thread.currentThread().getName());
    this.jsonString=jsonString;
    readjson(jsonString);
  }

  protected void setTexture(String texture){
      this.texture = texture;
  }

  public void readjson(String jsonString) {
    try {
      JSONObject object = new JSONObject(jsonString);

      String com = object.getString("comment");
      JSONArray vertices = object.getJSONArray("vertices");
      JSONObject b = vertices.getJSONObject(0);
      JSONArray values = b.getJSONArray("values");

      sommets = new float[values.length()];
      for (int i = 0; i < values.length(); i++) {
          sommets[i] = Float.valueOf(values.get(i).toString());
      }

      JSONArray connectivity = object.getJSONArray("connectivity");
      JSONObject a = connectivity.getJSONObject(0);

      JSONArray indices = a.getJSONArray("indices");

      tabIndex = new int[indices.length()];
      for (int i = 0; i < indices.length(); i++) {
          tabIndex[i] = Integer.valueOf(indices.get(i).toString());
      }

      Random rand = new Random();
      nbIndex = tabIndex.length / 3;
        double[] t = new double[nbIndex * 2];
        for(int i = 0 ; i < nbIndex*2 ; i++){
            t[i] = rand.nextDouble();
        }
        textures = Buffers.newDirectDoubleBuffer(t);

    }
    catch (Exception e) {

        e.printStackTrace();

    }
  }

  public void update(){}

  public void init( GL2 gl){
      gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
      vertexFB =Buffers.newDirectFloatBuffer(sommets);
      gl.glVertexPointer(3, GL2.GL_FLOAT, 0, vertexFB);
      indexBuf = Buffers.newDirectIntBuffer(tabIndex);
      loadTexture(texture, gl);
  }

  public void loadTexture(String file,GL2 gl){
    if(file != null) {
        try {
            newTex = TextureIO.newTexture(new File(file), false);
            newTex.enable(gl);
            newTex.bind(gl);
            newTex.disable(gl);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (NullPointerException e) {
          System.err.println(file);
          e.printStackTrace();
        }
    }
  }

  public void appliqueChangementRepere(GL2 gl){
  }

  public void appliqueTexture(GL2 gl){
    gl.glEnableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
    gl.glTexCoordPointer(2,GL2.GL_DOUBLE,0,textures);
    newTex.enable(gl);
    newTex.bind(gl);
    gl.glEnable(GL2.GL_TEXTURE_2D);
    gl.glTexEnvf(GL2.GL_TEXTURE_ENV,GL2.GL_TEXTURE_ENV_MODE,GL2.GL_REPLACE);

}


  public void affiche(GL2 gl){
      gl.glColor3f(0.0f,1.0f,1.0f);
      gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_FILL);
      gl.glPushMatrix();
      this.appliqueChangementRepere(gl);
      gl.glScaled(0.04,0.04,0.04);

      if(this.texture != null)
        this.appliqueTexture(gl);

      gl.glDrawElements(GL2.GL_TRIANGLES, nbIndex * 3, GL2.GL_UNSIGNED_INT, indexBuf);
      gl.glPopMatrix();
  }
}
