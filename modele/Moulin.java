package modele;

import com.jogamp.opengl.GL2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;




public class Moulin extends Maillage3D {

    private Pale pale;
    public Moulin(String jsonString) {
        super(jsonString);
        String jsonPale = "";
        try {
            jsonPale = new Scanner(new File("pales.json")).useDelimiter("\\Z").next();

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
        pale = new Pale(jsonPale, this);
        setTexture("./windmill/material/marron.jpg");



    }

    @Override
    public void init(GL2 gl) {
        super.init(gl);
        pale.init(gl);
    }

    @Override
    public void appliqueChangementRepere(GL2 gl) {
        gl.glRotated(90,-1,0,0);
        gl.glRotated(-90,0,0,1);
    }


    @Override
    public void update() {
      pale.update();
      this.angle += this.speed;
      this.Xangle += this.Xspeed;
    }

    @Override
    public void affiche(GL2 gl) {
      super.affiche(gl);
      // gl.glColor3f(0.5f, 0.35f, 0.5f);
      pale.affiche(gl);
    }
}
