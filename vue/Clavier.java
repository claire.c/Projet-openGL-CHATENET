package vue;
import controleur.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.*;



public class Clavier implements KeyListener{
    Controleur controleur;

    public  Clavier(Controleur c){
        controleur = c;
    }


    public void keyTyped(KeyEvent e){
        key(e.getKeyChar(), 1, 1);
    }




    void key(char key,int x,int y) {
        switch ( key ) {
            case 'h'  : controleur.deplacerCam(0.2,0,0);
                        break;
            case 'k'  : controleur.deplacerCam(-0.2,0,0);
                        break;
            case 'u'  : controleur.deplacerCam(0,0.2,0);
                        break;
            case 'j'  : controleur.deplacerCam(0,-0.2,0);
                        break;
            case '+'  : controleur.deplacerCam(0,0,-2);
                        break;
            case '-'  : controleur.deplacerCam(0,0,2);
                        break;

            // case 'v'  : controleur.changerCouleur(0,1,0);
            //             break;
            // case 'r'  : controleur.changerCouleur(1,0,0);
            //             break;
            // case 'b'  : controleur.changerCouleur(0,0,1);
            //             break;
            // case 'm'  : controleur.changerCouleur(1,0,1);
            //             break;


        }
    }

    public void keyPressed(KeyEvent e){
        int keyCode = e.getKeyCode();
        switch( keyCode ) {
            case KeyEvent.VK_PAGE_UP :
                break;
            case KeyEvent.VK_PAGE_DOWN :
                break;
            case KeyEvent.VK_UP:
                break;
            case KeyEvent.VK_DOWN:
                break;
            case KeyEvent.VK_LEFT:
                break;
            case KeyEvent.VK_RIGHT :
                break;
            case KeyEvent.VK_ESCAPE:
                System.exit(0);
                break;


        }


    }

    public void keyReleased(KeyEvent e){
    }

}
